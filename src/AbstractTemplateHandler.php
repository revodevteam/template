<?php
namespace Framework\Template;

use Framework\Pagination\PaginationInterface;
use Framework\Http\Notification\Notification;

abstract class AbstractTemplateHandler implements TemplateInterface
{

    protected $viewFile;
    protected $values;
    protected $messages;

    /**
     *
     * @var \Framework\Pagination\PaginationInterface 
     */
    protected $pagination;

    public final function __construct(string $viewFile)
    {
        $this->viewFile = $viewFile;
        $this->values = [];
        $this->messages = (new Notification())->flashNote();

        if (method_exists($this, 'boot')) {
            $this->boot();
        }
    }

    abstract public function preRender();

    abstract public function postRender();

    public final function render()
    {
        session_write_close();

        $this->preRender();

        if (file_exists($this->viewFile)) {
            require $this->viewFile;
        } else {
            throw new ViewFileNotFoundException($this->viewFile);
        }

        $this->PostRender();
    }

    public function with(array $args = [])
    {
        $this->values = $args;

        return $this;
    }

    public function __set($name, $value)
    {
        $this->values[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->values[$name])) {
            return $this->values[$name];
        }

        throw new \Exception($name . ' Variable not found !');
    }

    public function __isset($name)
    {
        return isset($this->values[$name]);
    }

    public function escape($value)
    {
        if (is_array($value)) {
            return array_map(function($value) {
                return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
            }, $value);
        }

        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    }

    public function escapeJson($value)
    {
        return json_encode($value, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);
    }

    public function csrfToken()
    {
        return '<input type="hidden" value="' . ($_SESSION['csrf_token'] ?? '') . '" name="csrf_token" />';
    }

    public function pagination(PaginationInterface $pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }

    public function message($name = null)
    {
        if ($name === null) {
            return $this->messages;
        }
        return $this->messages[$name] ?? false;
    }

    public function val($name, $default = false)
    {
        if (isset($this->values[$name])) {
            return $this->values[$name];
        }

        return $default;
    }

    public function objval($obj, $name, $default = false)
    {
        if (isset($this->values[$obj])) {
            if (isset($this->values[$obj]->$name)) {
                return $this->values[$obj]->$name;
            }
        }
        return $default;
    }
}
