<?php
namespace Framework\Template;

interface TemplateInterface
{
    public function render();

    public function with(array $args = []);
}
